! XTerm
XTerm*Title:           XTerm
XTerm.termName:        xterm-color
XTerm*scrollBar:       false
XTerm*saveLines:       65535
XTerm*metaSendsEscape: true
XTerm*hold:            false
XTerm*cursorBlink:     false
XTerm*sameName:        true
XTerm*waitForMap:      false
XTerm*deleteIsDEL:     false
XTerm*multiScroll:     true
XTerm*visualBell:      false

XTerm*locale:          true
XTerm*utf8:            3

XTerm*font:            -*-courier-medium-r-normal-*-10-*-*-*-*-*-iso10646-1
XTerm*italicFont:      -*-courier-medium-u-normal-*-10-*-*-*-*-*-iso10646-1
XTerm*boldfont:        -*-courier-medium-r-normal-*-10-*-*-*-*-*-iso10646-1
XTerm*bolditalicFont:  -*-courier-medium-u-normal-*-10-*-*-*-*-*-iso10646-1

XTerm*colorMode:       true
XTerm*colorBDMode:     false
XTerm*colorBLMode:     false
XTerm*colorULMode:     false
XTerm*colorRVMode:     false
XTerm*boldMode:        false
XTerm*boldColors:      true
XTerm*veryBoldColors:  0

XTerm*background:      #000000
XTerm*foreground:      #FFFFFF
XTerm*cursorColor:     #FFFF00
XTerm*colorBD:         #FFFFFF
XTerm*colorUL:         #DD0000

XTerm*color0:          #000000
XTerm*color1:          #CC0000
XTerm*color2:          #00CC00
XTerm*color3:          #bd5e01
XTerm*color4:          #0000CC
XTerm*color5:          #CC00CC
XTerm*color6:          #00CCCC
XTerm*color7:          #CCCCCC
XTerm*color8:          #666666
XTerm*color9:          #FF0000
XTerm*color10:         #00FF00
XTerm*color11:         #FFFF00
XTerm*color12:         #0000FF
XTerm*color13:         #FF00FF
XTerm*color14:         #00FFFF
XTerm*color15:         #FFFFFF


! All kinds of urxvt
URxvt.scrollBar:       false
URxvt.saveLines:       65535
URxvt.hold:            false
URxvt.cursorBlink:     false
URxvt.visualBell:      false
URxvt.internalBorder:  0
URxvt.externalBorder:  0

URxvt.font:            -*-courier-medium-r-normal-*-10-*-*-*-*-*-iso10646-1
URxvt.italicFont:      -*-courier-medium-o-normal-*-10-*-*-*-*-*-iso10646-1
URxvt.boldfont:        -*-courier-bold-r-normal-*-10-*-*-*-*-*-iso10646-1
URxvt.bolditalicFont:  -*-courier-bold-o-normal-*-10-*-*-*-*-*-iso10646-1

URxvt.intensityStyles: true

URxvt.background:      #000000
URxvt.foreground:      #FFFFFF
URxvt.cursorColor:     #FFFF00

URxvt.color0:          #000000
URxvt.color1:          #CC0000
URxvt.color2:          #00CC00
URxvt.color3:          #bd5e01
URxvt.color4:          #0000CC
URxvt.color5:          #CC00CC
URxvt.color6:          #00CCCC
URxvt.color7:          #CCCCCC
URxvt.color8:          #666666
URxvt.color9:          #FF0000
URxvt.color10:         #00FF00
URxvt.color11:         #FFFF00
URxvt.color12:         #0000FF
URxvt.color13:         #FF00FF
URxvt.color14:         #00FFFF
URxvt.color15:         #FFFFFF

! Terminal in urxvt
Term.perl-ext-common:  matcher,tabbedex,searchable-scrollback<M-s>
URxvt.backgroundPixmap: {{HOME}}/.urxvt/80-char.png;0x0+0+0:tile
URxvt.tabbed.new-button: false
URxvt.tabbed.autohide: ignore-named
URxvt.tabbed.bell-timeout: 0

! Irssi in urxvt
IRC.title:     IRC
IRC.saveLines: 0

! mpdshow in urxvt
MPD.title:     Music Player Daemon
MPD.saveLines: 0

! Emacs
emacs.menuBar: off
emacs.toolBar: off
emacs.verticalScrollBars: off
emacs.font: -*-courier-medium-r-normal-*-10-*-*-*-*-*-iso10646-1
emacs.leftFringe: 6
emacs.rightFringe: 6
emacs.cursorBlink: 0
emacs.fullscreen: fullheight
emacs.internalBorder: 0
emacs.borderWidth: 0
